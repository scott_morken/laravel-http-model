<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 9:43 AM
 */

namespace Smorken\HttpModel\Contracts\HttpClient;

use Psr\Http\Message\ResponseInterface;
use Smorken\HttpModel\HttpClient\ResponseException;

interface BodyProcessor
{

    /**
     * @param ResponseInterface $response
     * @return mixed
     * @throws ResponseException
     */
    public function process($response);
}
