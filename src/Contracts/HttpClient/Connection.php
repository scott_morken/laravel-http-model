<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 3:47 PM
 */

namespace Smorken\HttpModel\Contracts\HttpClient;

interface Connection
{

    /**
     * @param $params
     * @return array<array>
     */
    public function send($params);
}
