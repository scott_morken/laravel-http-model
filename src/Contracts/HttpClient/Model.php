<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 3:33 PM
 */

namespace Smorken\HttpModel\Contracts\HttpClient;

interface Model extends \Smorken\HttpModel\Contracts\Model
{

    /**
     * @param $parser_class
     */
    public function setParserClass($parser_class);

    /**
     * @return Parser|null
     */
    public function getParser();

    /**
     * @return string
     */
    public function getUri();

    /**
     * @param $uri
     * @return $this
     */
    public function setUri($uri);

    /**
     * @return string
     */
    public function getMethod();

    /**
     * @param $method
     * @return $this
     */
    public function setMethod($method);

    /**
     * Get a new request builder for the model.
     *
     * @param bool $add_defaults
     * @return \Smorken\HttpModel\HttpClient\Guzzle\Request
     */
    public function newRequest($add_defaults = true);

    /**
     * Get the database connection for the model.
     *
     * @return \Smorken\HttpModel\Contracts\HttpClient\Connection
     */
    public function getConnection();

    /**
     * Set the connection associated with the model.
     *
     * @param  Connection $connection
     * @return $this
     */
    public static function setConnection(Connection $connection);
}
