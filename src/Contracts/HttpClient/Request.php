<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 9:19 AM
 */

namespace Smorken\HttpModel\Contracts\HttpClient;

use Illuminate\Support\Collection;
use Smorken\HttpModel\HttpClient\RequestException;

interface Request extends \Smorken\HttpModel\Contracts\Request
{

    const QUERYSTRING = 1;
    const BODY = 2;

    /**
     * @param $uri
     * @return $this
     * @throws RequestException
     */
    public function uri($uri);

    /**
     * @return string
     * @throws RequestException
     */
    public function getUri();

    /**
     * GET
     * @return $this
     */
    public function get();

    /**
     * POST
     * @return $this
     */
    public function post();

    /**
     * PUT
     * @return $this
     */
    public function put();

    /**
     * PATCH
     * @return $this
     */
    public function patch();

    /**
     * DELETE
     * @return $this
     */
    public function delete();

    /**
     * @param string $method
     * @return $this
     */
    public function setMethod($method);

    /**
     * @return string
     */
    public function getMethod();

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function header($key, $value);

    /**
     * @param array $headers
     * @return $this
     */
    public function headers(array $headers);

    /**
     * @return array
     */
    public function getHeaders();

    /**
     * @param $key
     * @param $value
     * @param null $type
     * @return $this
     */
    public function param($key, $value, $type = null);

    /**
     * @param array $params
     * @param null $type
     * @return $this
     */
    public function params(array $params, $type = null);

    /**
     * @param null $type
     * @return array
     */
    public function getParams($type = null);

    /**
     * @return int
     */
    public function getDefaultParamType();

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function option($key, $value);

    /**
     * @param $options
     * @return $this
     */
    public function options($options);

    /**
     * @return array
     */
    public function getOptions();

    /**
     * sets the json flag on the request
     * @return $this
     */
    public function json();

    /**
     * @return bool
     */
    public function getJson();

    /**
     * ['json' => params] or ['form_params' => params] or null if no body
     * @return array|null
     */
    public function getBody();

    /**
     * @return string
     */
    public function buildUri();

    /**
     * @return array
     */
    public function toRequestOptions();
}
