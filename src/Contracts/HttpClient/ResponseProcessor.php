<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 9:42 AM
 */

namespace Smorken\HttpModel\Contracts\HttpClient;

use Illuminate\Contracts\Logging\Log;
use Psr\Http\Message\ResponseInterface;
use Smorken\HttpModel\HttpClient\ResponseException;

interface ResponseProcessor
{

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    public function isOk($response);

    /**
     * @param ResponseInterface $response
     */
    public function toError($response);

    /**
     * @param ResponseInterface $response
     * @return mixed
     * @throws ResponseException
     */
    public function process($response);

    /**
     * @return BodyProcessor
     */
    public function getBodyProcessor();

    /**
     * @return Log
     */
    public function getLogger();
}
