<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 3:59 PM
 */

namespace Smorken\HttpModel\Contracts;

use Smorken\Convertible\Contracts\VO;

interface Model extends VO
{

}
