<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 4:02 PM
 */

namespace Smorken\HttpModel\Contracts;

interface Parser
{

    /**
     * @param $response
     * @return mixed
     */
    public function parse($response);
}
