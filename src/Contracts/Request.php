<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 3:58 PM
 */

namespace Smorken\HttpModel\Contracts;

use Illuminate\Support\Collection;

interface Request
{

    /**
     * @return mixed
     */
    public function run();

    /**
     * @return \Smorken\HttpModel\Contracts\Model|null
     */
    public function first();

    /**
     * @return Collection<\Smorken\HttpModel\Contracts\Model>
     */
    public function all();

    /**
     * @param Model $model
     */
    public function setModel(\Smorken\HttpModel\Contracts\Model $model);

    /**
     * @return \Smorken\HttpModel\Contracts\Model
     */
    public function getModel();
}
