<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 5:17 PM
 */

namespace Smorken\HttpModel\Contracts\Soap;

use Phpro\SoapClient\Type\RequestInterface;

interface Client
{

    /**
     * @param $func
     * @param RequestInterface $request
     * @return mixed
     */
    public function request($func, RequestInterface $request);
}
