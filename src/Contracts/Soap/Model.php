<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:56 PM
 */

namespace Smorken\HttpModel\Contracts\Soap;

use Phpro\SoapClient\ClientBuilder;
use Phpro\SoapClient\ClientFactory;
use Phpro\SoapClient\Soap\ClassMap\ClassMapCollection;

interface Model extends \Smorken\HttpModel\Contracts\Model
{

    /**
     * @return string
     */
    public function getWsdl();

    /**
     * @param string $wsdl
     */
    public function setWsdl($wsdl);

    /**
     * @return ClassMapCollection
     */
    public function getClassMaps();

    /**
     * @param ClassMapCollection $classMaps
     */
    public function setClassMaps(ClassMapCollection $classMaps);

    /**
     * @param $parser_class
     */
    public function setParserClass($parser_class);

    /**
     * @return Parser|null
     */
    public function getParser();

    /**
     * The array key that will bring back the result set for this model
     * @return null|string
     */
    public function getResultKey();

    /**
     * The array key that will bring back the result set for this model
     * @param $key
     */
    public function setResultKey($key);

    /**
     * @param $data
     */
    public function setLastSoapRequest($data);

    /**
     * @return array|null
     */
    public function getLastSoapRequest();

    /**
     * Get a new request builder for the model.
     *
     * @return \Smorken\HttpModel\Contracts\Soap\Request
     */
    public function newRequest();

    /**
     * Get the client builder for the model.
     *
     * @return mixed
     */
    public function getClientBuilder();

    /**
     * Set the connection associated with the model.
     *
     * @param ClientBuilder $clientBuilder
     */
    public static function setClientBuilder(ClientBuilder $clientBuilder);

    /**
     * Get the client factory for the model.
     *
     * @return ClientFactory
     */
    public function getClientFactory();

    /**
     * Set the client factory
     *
     * @param ClientFactory $clientFactory
     */
    public static function setClientFactory(ClientFactory $clientFactory);
}
