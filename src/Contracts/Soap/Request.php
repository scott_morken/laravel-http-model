<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:56 PM
 */

namespace Smorken\HttpModel\Contracts\Soap;

use Phpro\SoapClient\ClientBuilder;
use Phpro\SoapClient\Middleware\MiddlewareInterface;
use Phpro\SoapClient\Type\RequestInterface;

interface Request extends \Smorken\HttpModel\Contracts\Request
{

    /**
     * @return ClientBuilder
     */
    public function getClientBuilder();

    /**
     * @param string $function
     * @param RequestInterface|null $request
     * @return $this
     */
    public function asFunction($function, RequestInterface $request = null);

    /**
     * @return string
     */
    public function getFunction();

    /**
     * @param RequestInterface $request
     * @return $this
     */
    public function request(RequestInterface $request);

    /**
     * @return RequestInterface
     */
    public function getRequest();

    /**
     * @param MiddlewareInterface $middleware
     * @return $this
     */
    public function middleware($middleware);

    /**
     * @return array
     */
    public function getMiddleware();

    /**
     * Store the last soap request debug info on the model
     */
    public function saveLastSoapRequest();
}
