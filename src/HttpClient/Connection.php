<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 4:11 PM
 */

namespace Smorken\HttpModel\HttpClient;

use Smorken\HttpModel\Contracts\HttpClient\ResponseProcessor;

abstract class Connection implements \Smorken\HttpModel\Contracts\HttpClient\Connection
{

    protected $client;

    /**
     * @var ResponseProcessor
     */
    protected $processor;

    protected $options = [];

    public function __construct($client, ResponseProcessor $processor, $options = [])
    {
        $this->setClient($client);
        $this->processor = $processor;
        $this->options = $options;
    }

    public function setClient($client)
    {
        $this->client = $client;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getResponseProcessor()
    {
        return $this->processor;
    }
}
