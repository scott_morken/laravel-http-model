<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 3:34 PM
 */

namespace Smorken\HttpModel\HttpClient\Guzzle;

use GuzzleHttp\Client;
use Smorken\Convertible\Models\VO;
use Smorken\HttpModel\Contracts\HttpClient\Connection;
use Smorken\HttpModel\Contracts\HttpClient\Parser;
use Smorken\HttpModel\HttpClient\GuzzleConnection;
use Smorken\HttpModel\HttpClient\Processors\Psr7\JsonProcessor;
use Smorken\HttpModel\HttpClient\Processors\Psr7\ResponseProcessor;

class Model extends VO implements \Smorken\HttpModel\Contracts\HttpClient\Model
{

    /**
     * connection name for the model
     * @var Connection
     */
    protected static $connection;

    /**
     * uri for connection
     * @var string
     */
    protected $uri;

    protected $method = 'get';

    /**
     * ['key' => 'required|query_string(body)']
     * @var array
     */
    protected $request_rules = [];

    protected $default_request_options = [];

    protected $parser_class;

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param $uri
     * @return $this
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param $method
     * @return $this
     */
    public function setMethod($method)
    {
        $method = strtolower($method);
        $allowed = ['get', 'post', 'put', 'patch', 'delete'];
        if (!in_array($method, $allowed)) {
            throw new \InvalidArgumentException("Method is not one of " . implode(', ', $allowed));
        }
        $this->method = $method;
        return $this;
    }

    /**
     * @param $parser_class
     */
    public function setParserClass($parser_class)
    {
        $this->parser_class = $parser_class;
    }

    /**
     * @return Parser|null
     */
    public function getParser()
    {
        if ($this->parser_class) {
            return new $this->parser_class;
        }
    }

    /**
     * Get a new request builder for the model.
     *
     * @param bool $add_defaults
     * @return \Smorken\HttpModel\HttpClient\Guzzle\Request
     */
    public function newRequest($add_defaults = true)
    {
        $conn = $this->getConnection();
        $builder = new \Smorken\HttpModel\HttpClient\Guzzle\Request($conn);
        $builder->uri($this->getUri());
        if ($this->method) {
            $builder->setMethod($this->getMethod());
        }
        if ($add_defaults && $this->default_request_options) {
            $builder->options($this->default_request_options);
        }
        return $builder->setModel($this);
    }

    /**
     * Get the database connection for the model.
     *
     * @return \Smorken\HttpModel\Contracts\HttpClient\Connection
     */
    public function getConnection()
    {
        if (!static::$connection) {
            $client = new Client(config('httpmodel.http.client_options', []));
            $bpclass = config('httpmodel.http.body_processor', JsonProcessor::class);
            $bp = new $bpclass;
            $rpclass = config('httpmodel.http.response_processor', ResponseProcessor::class);
            $rp = new $rpclass($bp);
            static::setConnection(new GuzzleConnection($client, $rp));
        }
        return static::$connection;
    }

    /**
     * Set the connection associated with the model.
     *
     * @param  Connection $connection
     * @return $this
     */
    public static function setConnection(Connection $connection)
    {
        static::$connection = $connection;
    }
}
