<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 9:45 AM
 */

namespace Smorken\HttpModel\HttpClient;

class GuzzleConnection extends Connection implements \Smorken\HttpModel\Contracts\HttpClient\Connection
{

    /**
     * @param $params
     * @return array<array>|null
     */
    public function send($params)
    {
        $response = call_user_func_array([$this->getClient(), 'request'], $params);
        return $this->getResponseProcessor()->process($response);
    }
}
