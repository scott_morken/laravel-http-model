<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 9:36 AM
 */

namespace Smorken\HttpModel\HttpClient\Processors\Psr7;

use Psr\Http\Message\ResponseInterface;
use Smorken\HttpModel\Contracts\HttpClient\BodyProcessor;
use Smorken\HttpModel\HttpClient\ResponseException;

class JsonProcessor implements BodyProcessor
{

    /**
     * @param ResponseInterface $response
     * @return mixed
     * @throws ResponseException
     */
    public function process($response)
    {
        $contents = $response->getBody()->getContents();
        $processed = json_decode($contents, true);
        if (is_null($processed)) {
            throw new ResponseException(json_last_error_msg(), json_last_error());
        }
        return $processed;
    }
}
