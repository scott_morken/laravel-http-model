<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 9:38 AM
 */

namespace Smorken\HttpModel\HttpClient\Processors\Psr7;

use Psr\Http\Message\ResponseInterface;
use Smorken\HttpModel\HttpClient\ResponseException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ResponseProcessor extends \Smorken\HttpModel\HttpClient\Processors\ResponseProcessor implements \Smorken\HttpModel\Contracts\HttpClient\ResponseProcessor
{

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    public function isOk($response)
    {
        return $response->getStatusCode() < 400;
    }

    /**
     * @param ResponseInterface $response
     * @throws ResponseException
     */
    public function toError($response)
    {
        $error_code = $response->getStatusCode();
        $msg = $response->getReasonPhrase();
        $body = $response->getBody()->getContents();
        switch ($error_code) {
            case 404:
                $e = new NotFoundHttpException($msg);
                break;
            case 401:
                $e = new AccessDeniedHttpException($msg);
                break;
            case 403:
                $e = new UnauthorizedHttpException($msg);
                break;
            default:
                $e = new ResponseException($msg, $error_code);
                $this->logger->error(
                    'Response error',
                    ['code' => $error_code, 'msg' => $msg, 'body' => $body]
                );
                break;
        }
        throw $e;
    }
}
