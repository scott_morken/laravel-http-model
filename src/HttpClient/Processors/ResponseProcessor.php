<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 9:52 AM
 */

namespace Smorken\HttpModel\HttpClient\Processors;

use Illuminate\Contracts\Logging\Log;
use Psr\Http\Message\ResponseInterface;
use Smorken\HttpModel\Contracts\HttpClient\BodyProcessor;
use Smorken\HttpModel\HttpClient\ResponseException;

abstract class ResponseProcessor implements \Smorken\HttpModel\Contracts\HttpClient\ResponseProcessor
{

    /**
     * @var Log
     */
    protected $logger;

    /**
     * @var BodyProcessor
     */
    protected $processor;

    /**
     * ResponseProcessor constructor.
     * @param BodyProcessor $processor
     * @param Log $logger
     */
    public function __construct(BodyProcessor $processor, Log $logger)
    {
        $this->logger = $logger;
        $this->processor = $processor;
    }

    /**
     * @return Log
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param ResponseInterface $response
     * @return mixed
     * @throws ResponseException
     */
    public function process($response)
    {
        if ($this->isOk($response)) {
            try {
                return $this->getBodyProcessor()->process($response);
            } catch (ResponseException $e) {
                $this->getLogger()->error($e);
                throw $e;
            }
        }
        return $this->toError($response);
    }

    /**
     * @return BodyProcessor
     */
    public function getBodyProcessor()
    {
        return $this->processor;
    }
}
