<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 4:39 PM
 */

namespace Smorken\HttpModel\HttpClient;

use Illuminate\Support\Collection;

class Request implements \Smorken\HttpModel\Contracts\HttpClient\Request
{

    protected $model;

    protected $method = 'get';

    protected $headers = [];

    protected $params = [];

    protected $request_options = [];

    protected $json = false;

    protected $uri;

    protected $default_param_type = self::QUERYSTRING;

    /**
     * @var \Smorken\HttpModel\Contracts\HttpClient\Connection
     */
    protected $connection;

    public function __construct(\Smorken\HttpModel\Contracts\HttpClient\Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param $uri
     * @return $this
     * @throws RequestException
     */
    public function uri($uri)
    {
        $uri = filter_var($uri, FILTER_SANITIZE_URL);
        if (!$uri) {
            throw new RequestException('Invalid URI.');
        }
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return string
     * @throws RequestException
     */
    public function getUri()
    {
        if (!$this->uri) {
            throw new RequestException('URI must be provided.');
        }
        return $this->uri;
    }

    /**
     * GET
     * @return $this
     */
    public function get()
    {
        $this->method = 'get';
        $this->default_param_type = self::QUERYSTRING;
        return $this;
    }

    /**
     * POST
     * @return $this
     */
    public function post()
    {
        $this->method = 'post';
        $this->default_param_type = self::BODY;
        return $this;
    }

    /**
     * PUT
     * @return $this
     */
    public function put()
    {
        $this->method = 'put';
        $this->default_param_type = self::BODY;
        return $this;
    }

    /**
     * PATCH
     * @return $this
     */
    public function patch()
    {
        $this->method = 'patch';
        $this->default_param_type = self::BODY;
        return $this;
    }

    /**
     * DELETE
     * @return $this
     */
    public function delete()
    {
        $this->method = 'delete';
        $this->default_param_type = self::QUERYSTRING;
        return $this;
    }

    /**
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $method = strtolower($method);
        $this->$method();
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function header($key, $value)
    {
        $this->headers[$key] = $value;
        return $this;
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function headers(array $headers)
    {
        foreach ($headers as $key => $value) {
            $this->header($key, $value);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param $key
     * @param $value
     * @param null $type
     * @return $this
     */
    public function param($key, $value, $type = null)
    {
        $this->params[$key] = ['value' => $value, 'type' => $type];
        return $this;
    }

    /**
     * @param array $params
     * @param null $type
     * @return $this
     */
    public function params(array $params, $type = null)
    {
        foreach ($params as $k => $v) {
            $this->param($k, $v, $type);
        }
        return $this;
    }

    /**
     * @param null $type
     * @return array
     */
    public function getParams($type = null)
    {
        $this->ensureTypedParams();
        if (is_null($type)) {
            $type = $this->getDefaultParamType();
        }
        $params = [];
        foreach ($this->params as $k => $data) {
            if ($data['type'] === $type || is_null($data['type'])) {
                $params[$k] = $data['value'];
            }
        }
        return $params;
    }

    /**
     * make sure that the params are where they belong
     * set nulls to default and check any that are set to make sure
     * they can belong to the type (eg, body doesn't exist on a get request)
     */
    protected function ensureTypedParams()
    {
        $default_type = $this->getDefaultParamType();
        foreach ($this->params as $key => $data) {
            if (is_null($data['type'])) {
                $this->params[$key]['type'] = $default_type;
            }
            if ($default_type === self::QUERYSTRING && $data['type'] === self::BODY) {
                $this->params[$key]['type'] = self::QUERYSTRING;
            }
        }
    }

    /**
     * @return int
     */
    public function getDefaultParamType()
    {
        return $this->default_param_type;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function option($key, $value)
    {
        $this->request_options[$key] = $value;
        return $this;
    }

    /**
     * @param $options
     * @return $this
     */
    public function options($options)
    {
        foreach ($options as $k => $v) {
            $this->option($k, $v);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->request_options;
    }

    /**
     * sets the json flag on the request
     * @return $this
     */
    public function json()
    {
        $this->json = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * ['json' => params] or ['form_params' => params] or null if no body
     * @return array|null
     */
    public function getBody()
    {
        if ($this->default_param_type === self::BODY) {
            $params = $this->getParams(self::BODY);
            if ($params) {
                if ($this->getJson()) {
                    return ['json', $params];
                } else {
                    return ['form_params', $params];
                }
            }
        }
        return null;
    }

    /**
     * @return string
     */
    public function buildUri()
    {
        $qs = $this->getParams(self::QUERYSTRING);
        if (count($qs)) {
            return sprintf('%s?%s', $this->getUri(), http_build_query($qs, null, '&'));
        }
        return $this->getUri();
    }

    /**
     * @return array
     */
    public function toRequestOptions()
    {
        $options = [
            $this->getMethod(),
            $this->buildUri(),
            [],
        ];
        $headers = $this->getHeaders();
        $this->addOption('headers', $headers, $options);
        $body = $this->getBody();
        if (!is_null($body)) {
            $this->addOption($body[0], $body[1], $options);
        }
        foreach ($this->getOptions() as $k => $v) {
            $this->addOption($k, $v, $options);
        }
        return $options;
    }

    protected function addOption($k, $v, &$options)
    {
        if ($v) {
            $options[2][$k] = $v;
        }
    }

    /**
     * @return array<array>|null
     */
    public function run()
    {
        $options = $this->toRequestOptions();
        $results = $this->connection->send($options);
        $parser = $this->getParser();
        if ($parser) {
            return $parser->parse($results);
        }
        return $results;
    }

    /**
     * Handles invalid method calls to avoid having an uncatchable error
     * @param $key
     * @param $params
     * @throws RequestMethodException
     */
    public function __call($key, $params)
    {
        throw new RequestMethodException("$key is not a valid method.");
    }

    /**
     * @return \Smorken\HttpModel\Contracts\HttpClient\Model|null
     */
    public function first()
    {
        $this->checkModel();
        $results = $this->run();
        if ($results && is_array($results)) {
            $result = array_shift($results);
            if ($result) {
                return $this->getModel()->newInstance($result);
            }
        }
        return null;
    }

    /**
     * @return Collection<\Smorken\HttpModel\Contracts\HttpClient\Model>
     */
    public function all()
    {
        $this->checkModel();
        $results = $this->run();
        $collection = new Collection();
        if (is_array($results)) {
            foreach ($results as $result) {
                $collection->push($this->getModel()->newInstance($result));
            }
        }
        return $collection;
    }

    public function setModel(\Smorken\HttpModel\Contracts\Model $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return \Smorken\HttpModel\Contracts\HttpClient\Model
     */
    public function getModel()
    {
        return $this->model;
    }

    protected function checkModel()
    {
        if (!$this->getModel()) {
            throw new ResponseException("Model must be set.");
        }
    }

    protected function getParser()
    {
        if ($this->getModel()) {
            return $this->getModel()->getParser();
        }
    }
}
