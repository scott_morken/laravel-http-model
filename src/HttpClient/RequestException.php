<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 7:43 AM
 */

namespace Smorken\HttpModel\HttpClient;

class RequestException extends \Exception implements \Smorken\HttpModel\Contracts\RequestException
{

}
