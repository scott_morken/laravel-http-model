<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 11:38 AM
 */

namespace Smorken\HttpModel\HttpClient;

class ResponseException extends \Exception implements \Smorken\HttpModel\Contracts\ResponseException
{

}
