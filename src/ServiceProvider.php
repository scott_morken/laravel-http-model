<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 3:25 PM
 */

namespace Smorken\HttpModel;

use GuzzleHttp\Client;
use Illuminate\Contracts\Logging\Log;
use Phpro\SoapClient\ClientFactory;
use Phpro\SoapClient\ClientFactoryInterface;
use Smorken\HttpModel\Contracts\HttpClient\Connection;
use Smorken\HttpModel\HttpClient\Guzzle\Model;
use Smorken\HttpModel\HttpClient\GuzzleConnection;
use Smorken\HttpModel\HttpClient\Processors\Psr7\JsonProcessor;
use Smorken\HttpModel\HttpClient\Processors\Psr7\ResponseProcessor;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        $this->bootConfig();
        Model::setConnection($this->app[Connection::class]);
        \Smorken\HttpModel\Soap\Phpro\Model::setClientFactory($this->app[ClientFactoryInterface::class]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            Connection::class,
            function ($app) {
                $client = new Client($app['config']->get('httpmodel.client_options', []));
                $bpclass = $app['config']->get('httpmodel.body_processor', JsonProcessor::class);
                $rpclass = $app['config']->get('httpmodel.response_processor', ResponseProcessor::class);
                $rp = new $rpclass(new $bpclass, $app[Log::class]);
                return new GuzzleConnection($client, $rp);
            }
        );
        $this->app->bind(
            ClientFactoryInterface::class,
            function ($app) {
                return new ClientFactory(\Smorken\HttpModel\Soap\Phpro\Client::class);
            }
        );
    }

    protected function bootConfig()
    {
        $config = __DIR__ . '/../config/config.php';
        $this->mergeConfigFrom($config, 'httpmodel');
        $this->publishes([$config => config_path('httpmodel.php')], 'config');
    }
}
