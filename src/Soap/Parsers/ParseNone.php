<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 2:32 PM
 */

namespace Smorken\HttpModel\Soap\Parsers;

use Smorken\HttpModel\Contracts\Soap\Parser;

class ParseNone implements Parser
{

    /**
     * @param $response
     * @return array<array>|null
     */
    public function parse($response)
    {
        return $response;
    }
}
