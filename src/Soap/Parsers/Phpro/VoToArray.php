<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 3:12 PM
 */

namespace Smorken\HttpModel\Soap\Parsers\Phpro;

use Phpro\SoapClient\Type\ResultInterface;
use Smorken\HttpModel\Contracts\Soap\Parser;

class VoToArray implements Parser
{

    protected $attribute_methods = [];

    /**
     * @param $response
     * @return array<array>|null
     */
    public function parse($response)
    {
        return $this->toArray($response);
    }

    protected function toArray($response)
    {
        $result = [];
        if (!$response) {
            return null;
        }
        if (is_object($response)) {
            return $this->getAttributesFromObject($response);
        } elseif (is_array($response)) {
            foreach ($response as $i => $o) {
                $result[$i] = $this->getAttributesFromObject($o);
            }
        } else {
            return $response;
        }
        return $result;
    }

    protected function getAttributesFromObject($obj)
    {
        if ($obj) {
            if (is_object($obj)) {
                return $this->handleObject($obj);
            } elseif (is_array($obj)) {
                return $this->toArray($obj);
            } else {
                return $obj;
            }
        }
        return [];
    }

    protected function handleObject($obj)
    {
        if (method_exists($obj, 'getResult')) {
            return $this->getAttributesFromObject($obj->getResult());
        }
        if ($obj instanceof \stdClass || $obj instanceof ResultInterface) {
            $attributes = [];
            foreach ($this->getAttributeMethods($obj) as $attr => $method) {
                $value = $this->getValue($obj, $attr, $method);
                $attributes[$attr] = $this->getValueToAdd($value);
            }
            return $attributes;
        }
        return $obj;
    }

    protected function getValue($obj, $attr, $method)
    {
        $value = null;
        if ($method) {
            $value = $obj->$method();
        } elseif (is_null($method)) {
            $value = $obj->$attr;
        }
        return $value;
    }

    protected function getValueToAdd($value)
    {
        if (is_object($value)) {
            return $this->getAttributesFromObject($value);
        } elseif (is_array($value)) {
            return $this->toArray($value);
        }
        return $value;
    }

    protected function getAttributeMethods($obj)
    {
        $cls = get_class($obj);
        if (!isset($this->attribute_methods[$cls])) {
            $this->attribute_methods[$cls] = [];
            foreach (get_class_methods($obj) as $method) {
                if (strpos($method, 'get') === 0) {
                    $attribute = substr($method, 3);
                    $this->attribute_methods[$cls][$attribute] = $method;
                }
            }
            foreach (get_object_vars($obj) as $property => $v) {
                if ($v) {
                    $this->attribute_methods[$cls][$property] = null;
                }
            }
        }
        return $this->attribute_methods[$cls];
    }
}
