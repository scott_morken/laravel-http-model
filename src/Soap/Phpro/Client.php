<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:31 PM
 */

namespace Smorken\HttpModel\Soap\Phpro;

use Phpro\SoapClient\ClientInterface;
use Phpro\SoapClient\Type\RequestInterface;

class Client extends \Phpro\SoapClient\Client implements ClientInterface, \Smorken\HttpModel\Contracts\Soap\Client
{

    public function request($func, RequestInterface $request)
    {
        return $this->call($func, $request);
    }
}
