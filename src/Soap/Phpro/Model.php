<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:15 PM
 */

namespace Smorken\HttpModel\Soap\Phpro;

use Phpro\SoapClient\ClientBuilder;
use Phpro\SoapClient\ClientFactory;
use Phpro\SoapClient\Soap\ClassMap\ClassMapCollection;
use Phpro\SoapClient\Soap\Handler\GuzzleHandle;
use Smorken\Convertible\Models\VO;
use Smorken\HttpModel\Contracts\Soap\Parser;
use Smorken\HttpModel\Soap\Parsers\Phpro\VoToArray;

class Model extends VO implements \Smorken\HttpModel\Contracts\Soap\Model
{

    /**
     * @var string
     */
    protected $wsdl;

    /**
     * @var ClassMapCollection
     */
    protected $classMaps;

    /**
     * class name of provider
     * @var string
     */
    protected $classMapProvider;

    /**
     * @var string
     */
    protected $parser_class = VoToArray::class;

    /**
     * @var string|null
     */
    protected $result_key;

    /**
     * @var array
     */
    protected $last_soap_request;

    /**
     * @var ClientBuilder
     */
    protected static $clientBuilder;

    /**
     * @var ClientFactory
     */
    protected static $clientFactory;

    /**
     * @return string
     */
    public function getWsdl()
    {
        return $this->wsdl;
    }

    /**
     * @param string $wsdl
     */
    public function setWsdl($wsdl)
    {
        $this->wsdl = $wsdl;
    }

    /**
     * @return ClassMapCollection
     */
    public function getClassMaps()
    {
        if (!$this->classMaps && $this->classMapProvider) {
            $this->classMaps = call_user_func([$this->classMapProvider, 'get']);
        }
        return $this->classMaps;
    }

    /**
     * @param ClassMapCollection $classMaps
     */
    public function setClassMaps(ClassMapCollection $classMaps)
    {
        $this->classMaps = $classMaps;
    }

    /**
     * @param $parser_class
     */
    public function setParserClass($parser_class)
    {
        $this->parser_class = $parser_class;
    }

    /**
     * @return Parser|null
     */
    public function getParser()
    {
        if ($this->parser_class) {
            return new $this->parser_class;
        }
    }

    /**
     * The array key that will bring back the result set for this model
     * @return null|string
     */
    public function getResultKey()
    {
        return $this->result_key;
    }

    /**
     * The array key that will bring back the result set for this model
     * @param $key
     */
    public function setResultKey($key)
    {
        $this->result_key = $key;
    }

    /**
     * @param $data
     */
    public function setLastSoapRequest($data)
    {
        $this->last_soap_request = $data;
    }

    /**
     * @return array|null
     */
    public function getLastSoapRequest()
    {
        return $this->last_soap_request;
    }

    /**
     * Get a new request builder for the model.
     *
     * @return \Smorken\HttpModel\Contracts\Soap\Request
     */
    public function newRequest()
    {
        $cb = $this->getClientBuilder();
        if ($this->getClassMaps()) {
            $cb->withClassMaps($this->getClassMaps());
        }
        $builder = new Request($cb);
        return $builder->setModel($this);
    }

    /**
     * Get the client builder for the model.
     *
     * @return mixed
     */
    public function getClientBuilder()
    {
        if (!static::$clientBuilder) {
            $opts = config('httpmodel.soap.soap_options', []);
            $cb = new ClientBuilder($this->getClientFactory(), $this->getWsdl(), $opts);
            $client_opts = config('httpmodel.soap.client_options', []);
            $cb->withHandler(GuzzleHandle::createForClient(new \GuzzleHttp\Client($client_opts)));
            static::setClientBuilder($cb);
        }
        return static::$clientBuilder;
    }

    /**
     * Set the connection associated with the model.
     *
     * @param ClientBuilder $clientBuilder
     */
    public static function setClientBuilder(ClientBuilder $clientBuilder)
    {
        static::$clientBuilder = $clientBuilder;
    }

    /**
     * Get the client factory for the model.
     *
     * @return mixed
     */
    public function getClientFactory()
    {
        if (!static::$clientFactory) {
            $factory = new ClientFactory(Client::class);
            static::setClientFactory($factory);
        }
        return static::$clientFactory;
    }

    /**
     * Set the client factory
     *
     * @param ClientFactory $clientFactory
     */
    public static function setClientFactory(ClientFactory $clientFactory)
    {
        static::$clientFactory = $clientFactory;
    }
}
