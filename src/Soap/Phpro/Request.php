<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:25 PM
 */

namespace Smorken\HttpModel\Soap\Phpro;

use Illuminate\Support\Collection;
use Phpro\SoapClient\ClientBuilder;
use Phpro\SoapClient\Middleware\MiddlewareInterface;
use Phpro\SoapClient\Type\RequestInterface;
use Smorken\HttpModel\Soap\RequestException;
use Smorken\HttpModel\Soap\ResponseException;

class Request implements \Smorken\HttpModel\Contracts\Soap\Request
{

    /**
     * @var ClientBuilder
     */
    protected $clientBuilder;

    /**
     * @var \Smorken\HttpModel\Contracts\Soap\Model
     */
    protected $model;

    protected $function;

    /**
     * @var RequestInterface
     */
    protected $request;

    protected $middleware = [];

    protected $saveLast = true;

    public function __construct(ClientBuilder $clientBuilder)
    {
        $this->clientBuilder = $clientBuilder;
    }

    /**
     * @return ClientBuilder
     */
    public function getClientBuilder()
    {
        return $this->clientBuilder;
    }

    /**
     * @param string $function
     * @param RequestInterface|null $request
     * @return $this
     */
    public function asFunction($function, RequestInterface $request = null)
    {
        $this->function = $function;
        if (!is_null($request)) {
            $this->request($request);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @param RequestInterface $request
     * @return $this
     */
    public function request(RequestInterface $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param MiddlewareInterface $middleware
     * @return $this
     */
    public function middleware($middleware)
    {
        $this->middleware[] = $middleware;
        return $this;
    }

    /**
     * @return array
     */
    public function getMiddleware()
    {
        return $this->middleware;
    }

    /**
     * Store the last soap request debug info on the model
     */
    public function saveLastSoapRequest()
    {
        $this->saveLast = true;
        return $this;
    }

    /**
     * @return array<array>|null
     */
    public function run()
    {
        if ($this->verify()) {
            $this->updateClientBuilder();
            $client = $this->getClientBuilder()->build();
            $results = $client->request($this->getFunction(), $this->getRequest());
            $parser = $this->getParser();
            if ($parser) {
                $results = $parser->parse($results);
            }
            $result_key = $this->getModel()->getResultKey();
            if ($this->saveLast) {
                $this->getModel()->setLastSoapRequest($client->debugLastSoapRequest());
            }
            if ($result_key && is_array($results) && array_key_exists($result_key, $results)) {
                return $results[$result_key];
            }
            return $results;
        }
        return null;
    }

    /**
     * @return \Smorken\HttpModel\Contracts\Soap\Model|null
     */
    public function first()
    {
        $results = $this->run();
        if ($results && is_array($results)) {
            $result = array_shift($results);
            if ($result) {
                return $this->getModel()->newInstance($result);
            }
        }
        return null;
    }

    /**
     * @return Collection<\Smorken\HttpModel\Contracts\Soap\Model>
     */
    public function all()
    {
        $results = $this->run();
        $collection = new Collection();
        if (is_array($results)) {
            foreach ($results as $result) {
                $collection->push($this->getModel()->newInstance($result));
            }
        }
        return $collection;
    }

    protected function updateClientBuilder()
    {
        foreach ($this->getMiddleware() as $mw) {
            $this->getClientBuilder()->addMiddleware($mw);
        }
    }

    protected function verify()
    {
        if (!$this->getFunction()) {
            throw new RequestException('No function provided.');
        }
        if (!$this->getModel()->getWsdl()) {
            throw new RequestException('No WSDL provided.');
        }
        return true;
    }

    public function setModel(\Smorken\HttpModel\Contracts\Model $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return \Smorken\HttpModel\Contracts\Soap\Model
     * @throws ResponseException
     */
    public function getModel()
    {
        if (!$this->model) {
            throw new \UnexpectedValueException("Model must be set.");
        }
        return $this->model;
    }

    protected function getParser()
    {
        if ($this->getModel()) {
            return $this->getModel()->getParser();
        }
    }
}
