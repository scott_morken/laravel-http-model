<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 1:57 PM
 */

namespace Tests\Smorken\HttpModel\integration\JsonToModel;

use GuzzleHttp\Client;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Collection;
use Mockery as m;
use Psr\Http\Message\ResponseInterface;
use Smorken\HttpModel\HttpClient\Guzzle\Model;
use Smorken\HttpModel\HttpClient\GuzzleConnection;
use Smorken\HttpModel\HttpClient\Processors\Psr7\JsonProcessor;
use Smorken\HttpModel\HttpClient\Processors\Psr7\ResponseProcessor;

class JsonToModelTest extends \PHPUnit_Framework_TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testFirstModelFromResponse()
    {
        list($model, $client, $logger) = $this->getSut();
        $r = $this->mockResponse(200, [['foo' => 'bar']]);
        $client->shouldReceive('request')->once()->with('get', 'foo/bar', [])->andReturn($r);
        $m = $model->newRequest()->get()->first();
        $this->assertInstanceOf(ModelStub::class, $m);
        $this->assertEquals('bar', $m->foo);
    }

    public function testAllModelsFromResponse()
    {
        list($model, $client, $logger) = $this->getSut();
        $r = $this->mockResponse(200, [['foo' => 'bar'], ['fiz' => 'buz']]);
        $client->shouldReceive('request')->once()->with('get', 'foo/bar', [])->andReturn($r);
        $coll = $model->newRequest()->get()->all();
        $this->assertInstanceOf(Collection::class, $coll);
        $this->assertCount(2, $coll);
        $this->assertInstanceOf(ModelStub::class, $coll->first());
        $this->assertEquals('bar', $coll->first()->foo);
        $this->assertEquals('buz', $coll->last()->fiz);
    }

    protected function getSut()
    {
        $client = m::mock(Client::class);
        $logger = m::mock(Log::class);
        $conn = $this->getConnection($client, $logger);
        Model::setConnection($conn);
        $model = new ModelStub();
        $model->setUri('foo/bar');
        return [$model, $client, $logger];
    }

    protected function getConnection($client, $logger)
    {
        $bp = new JsonProcessor();
        $rp = new ResponseProcessor($bp, $logger);
        $conn = new GuzzleConnection($client, $rp);
        return $conn;
    }

    protected function mockResponse($code, $body, $message = '')
    {
        $r = m::mock(ResponseInterface::class);
        $r->shouldReceive('getStatusCode')->andReturn($code);
        $r->shouldReceive('getReasonPhrase')->andReturn($message);
        $r->shouldReceive('getBody->getContents')->andReturn(\json_encode($body));
        return $r;
    }
}

class ModelStub extends Model
{

}
