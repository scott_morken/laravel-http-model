<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 4:13 PM
 */

namespace Tests\Smorken\HttpModel\integration\Soap\Phpro;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Mockery as m;
use Phpro\SoapClient\ClientBuilder;
use Phpro\SoapClient\ClientFactory;
use Phpro\SoapClient\Soap\ClassMap\ClassMap;
use Phpro\SoapClient\Soap\ClassMap\ClassMapCollection;
use Phpro\SoapClient\Soap\Handler\GuzzleHandle;
use Phpro\SoapClient\Type\RequestInterface;
use Phpro\SoapClient\Type\ResultInterface;
use Phpro\SoapClient\Type\ResultProviderInterface;
use Smorken\HttpModel\Contracts\Soap\ClassMapper;
use Smorken\HttpModel\Soap\Phpro\Model;

class ModelTest extends \PHPUnit_Framework_TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testGetRunResults()
    {
        $gclient = $this->getClient([200 => $this->getResponseXml()]);
        list($m, $cb) = $this->getModel(ModelStub1::class, $gclient);
        $r = new GetHolidaysAvailable('UnitedStates');
        $result = $m->newRequest()->asFunction('GetHolidaysAvailable', $r)->run();
        $first = [
            'Code'        => 'NEW-YEARS-DAY-ACTUAL',
            'Description' => 'New Year\'s Day',
        ];
        $this->assertCount(34, $result);
        $this->assertEquals($first, $result[0]);
    }

    public function testGetAllModels()
    {
        $gclient = $this->getClient([200 => $this->getResponseXml()]);
        list($m, $cb) = $this->getModel(ModelStub1::class, $gclient);
        $r = new GetHolidaysAvailable('UnitedStates');
        $result = $m->newRequest()->asFunction('GetHolidaysAvailable', $r)->all();
        $this->assertCount(34, $result);
        $first = $result->first();
        $this->assertEquals('NEW-YEARS-DAY-ACTUAL', $first->Code);
        $this->assertEquals("New Year's Day", $first->Description);
    }

    public function testGetFirstModel()
    {
        $gclient = $this->getClient([200 => $this->getResponseXml()]);
        list($m, $cb) = $this->getModel(ModelStub1::class, $gclient);
        $r = new GetHolidaysAvailable('UnitedStates');
        $first = $m->newRequest()->asFunction('GetHolidaysAvailable', $r)->first();
        $this->assertEquals('NEW-YEARS-DAY-ACTUAL', $first->Code);
        $this->assertEquals("New Year's Day", $first->Description);
    }

    public function testSaveLastSoapRequest()
    {
        $gclient = $this->getClient([200 => $this->getResponseXml()]);
        list($m, $cb) = $this->getModel(ModelStub1::class, $gclient);
        $r = new GetHolidaysAvailable('UnitedStates');
        $first = $m->newRequest()->asFunction('GetHolidaysAvailable', $r)->saveLastSoapRequest()->first();
        $lastrequest = $m->getLastSoapRequest();
        $this->assertStringStartsWith(
            "User-Agent: GuzzleHttp/6.2.1 curl/7.51.0 PHP/7.0.20Host: www.holidaywebservice.comContent-Length: 333SOAPAction: http://www.holidaywebservice.com/HolidayService_v2/GetHolidaysAvailableContent-Type: text/xml; charset=\"utf-8\""
            ,
            filter_var($lastrequest['request']['headers'], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW)
        );
    }

    protected function getModel($model_cls, Client $client)
    {
        /**
         * @var Model $m
         */
        $m = new $model_cls;
        $cb = $this->getClientBuilder($client);
        $m->setWsdl($this->getWsdl());
        $model_cls::setClientBuilder($cb);
        return [$m, $cb];
    }

    protected function getWsdl()
    {
        return __DIR__ . '/../../fixtures/test.wsdl';
    }

    protected function getResponseXml()
    {
        return file_get_contents(__DIR__ . '/../../fixtures/test_response.xml');
    }

    protected function getRequestXml()
    {
        return file_get_contents(__DIR__ . '/../../fixtures/test_request.xml');
    }

    protected function getClientBuilder(Client $client)
    {
        $wsdl = $this->getWsdl();
        $cf = new ClientFactory(\Smorken\HttpModel\Soap\Phpro\Client::class);
        $cb = new ClientBuilder($cf, $wsdl, ['cache_wsdl' => WSDL_CACHE_NONE]);
        $cb->withHandler(GuzzleHandle::createForClient($client));
        return $cb;
    }

    protected function getClient($responses = [])
    {
        $rs = [];
        foreach ($responses as $code => $data) {
            $rs[] = new Response($code, [], $data);
        }
        $handler = new HandlerStack(new MockHandler($rs));
        return new Client(['handler' => $handler]);
    }
}

class ModelStub1 extends Model
{

    protected $result_key = 'HolidayCode';

    protected $classMapProvider = HolidayClassMap::class;
}

class HolidayClassMap implements ClassMapper
{

    /**
     * @return mixed
     */
    public static function get()
    {
        return new ClassMapCollection(
            [
                new ClassMap('GetHolidaysAvailable', GetHolidaysAvailable::class),
                new ClassMap('GetHolidaysAvailableResponse', GetHolidaysAvailableResponse::class),
                new ClassMap('GetHolidaysAvailableResult', GetHolidaysAvailableResult::class),
                new ClassMap('HolidayCode', HolidayCode::class),
            ]
        );
    }
}

class GetHolidaysAvailable implements RequestInterface
{

    protected $countryCode;

    public function __construct($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    public function getcountryCode()
    {
        return $this->countryCode;
    }
}

class GetHolidaysAvailableResponse implements ResultProviderInterface
{

    /**
     * @var GetHolidaysAvailableResult
     */
    protected $GetHolidaysAvailableResult = null;

    /**
     * @return GetHolidaysAvailableResult
     */
    public function getGetHolidaysAvailableResult()
    {
        return $this->GetHolidaysAvailableResult;
    }

    public function getResult()
    {
        return $this->getGetHolidaysAvailableResult();
    }
}

class GetHolidaysAvailableResult implements ResultInterface
{

    protected $HolidayCode = null;

    public function getHolidayCode()
    {
        return $this->HolidayCode;
    }
}

class HolidayCode implements ResultInterface
{

    protected $Code = null;
    protected $Description = null;

    public function getCode()
    {
        return $this->Code;
    }

    public function getDescription()
    {
        return $this->Description;
    }
}
