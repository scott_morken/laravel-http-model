<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 12:55 PM
 */

namespace Tests\Smorken\HttpModel\unit\HttpClient\Guzzle;

use Mockery as m;
use Smorken\HttpModel\Contracts\HttpClient\Connection;
use Smorken\HttpModel\HttpClient\Guzzle\Model;
use Smorken\HttpModel\HttpClient\Guzzle\Request;

class ModelTest extends \PHPUnit_Framework_TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testInvalidSetMethodIsException()
    {
        list($sut, $conn) = $this->getSut();
        $this->expectException(\InvalidArgumentException::class);
        $sut->setMethod('fiz');
    }

    public function testGetConnection()
    {
        list($sut, $conn) = $this->getSut();
        $this->assertEquals($conn, $sut->getConnection());
    }

    public function testNewRequestRequiresURI()
    {
        list($sut, $conn) = $this->getSut();
        $sut->setUri(null);
        $this->expectExceptionMessage('Invalid URI');
        $r = $sut->newRequest();
    }

    public function testNewRequestReturnsRequest()
    {
        list($sut, $conn) = $this->getSut();
        $r = $sut->newRequest();
        $this->assertInstanceOf(Request::class, $r);
        $this->assertEquals('foo', $r->getUri());
        $this->assertEquals('get', $r->getMethod());
    }

    public function testNewRequestWithMethodReturnsRequestWithMethod()
    {
        list($sut, $conn) = $this->getSut();
        $sut->setMethod('post');
        $r = $sut->newRequest();
        $this->assertEquals('post', $r->getMethod());
    }

    public function testNewRequestWithDefaultOptions()
    {
        $this->getSut();
        $sut = new ModelStub();
        $r = $sut->newRequest();
        $this->assertEquals(['foo' => 'bar'], $r->getOptions());
    }

    public function testNewRequestWithoutDefaultOptions()
    {
        $this->getSut();
        $sut = new ModelStub();
        $r = $sut->newRequest(false);
        $this->assertEquals([], $r->getOptions());
    }

    protected function getSut($uri = 'foo')
    {
        $conn = m::mock(Connection::class);
        Model::setConnection($conn);
        $sut = new Model();
        $sut->setUri($uri);
        return [$sut, $conn];
    }
}

class ModelStub extends Model
{
    protected $default_request_options = ['foo' => 'bar'];

    protected $uri = 'bar';

    protected $method = 'put';
}
