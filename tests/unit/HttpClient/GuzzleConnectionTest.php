<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 12:24 PM
 */

namespace Tests\Smorken\HttpModel\unit\HttpClient;

use Mockery as m;
use Smorken\HttpModel\Contracts\HttpClient\ResponseProcessor;
use Smorken\HttpModel\HttpClient\GuzzleConnection;

class GuzzleConnectionTest extends \PHPUnit_Framework_TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testSendPassesThroughToClient()
    {
        list($sut, $c, $p) = $this->getSut();
        $c->shouldReceive('request')->with('foo', 'bar')->andReturn('response');
        $p->shouldReceive('process')->with('response')->andReturn('processed');
        $this->assertEquals('processed', $sut->send(['foo', 'bar']));
    }

    protected function getSut()
    {
        $c = m::mock('Client');
        $p = m::mock(ResponseProcessor::class);
        $sut = new GuzzleConnection($c, $p);
        return [$sut, $c, $p];
    }
}
