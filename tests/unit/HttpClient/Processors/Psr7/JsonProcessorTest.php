<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 11:59 AM
 */

namespace Tests\Smorken\HttpModel\unit\HttpClient\Processors\Psr7;

use Psr\Http\Message\ResponseInterface;
use Smorken\HttpModel\HttpClient\Processors\Psr7\JsonProcessor;
use Mockery as m;
use Smorken\HttpModel\HttpClient\ResponseException;

class JsonProcessorTest extends \PHPUnit_Framework_TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testJsonInResponse()
    {
        $r = m::mock(ResponseInterface::class);
        $r->shouldReceive('getBody->getContents')->once()->andReturn('{"foo":"bar"}');
        $sut = $this->getSut();
        $this->assertEquals(['foo' => 'bar'], $sut->process($r));
    }

    public function testInvalidJsonInResponse()
    {
        $r = m::mock(ResponseInterface::class);
        $r->shouldReceive('getBody->getContents')->once()->andReturn('<xml></xml>');
        $sut = $this->getSut();
        $this->expectException(ResponseException::class);
        $sut->process($r);
    }

    protected function getSut()
    {
        return new JsonProcessor();
    }
}
