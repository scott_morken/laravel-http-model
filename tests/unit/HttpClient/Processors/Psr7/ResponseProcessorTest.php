<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 12:07 PM
 */

namespace Tests\Smorken\HttpModel\unit\HttpClient\Processors\Psr7;
use Illuminate\Contracts\Logging\Log;
use Mockery as m;
use Psr\Http\Message\ResponseInterface;
use Smorken\HttpModel\Contracts\HttpClient\BodyProcessor;
use Smorken\HttpModel\HttpClient\Processors\Psr7\ResponseProcessor;
use Smorken\HttpModel\HttpClient\ResponseException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ResponseProcessorTest extends \PHPUnit_Framework_TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testResponseNotOkIsException()
    {
        $r = m::mock(ResponseInterface::class);
        list($sut, $p, $l) = $this->getSut();
        $r->shouldReceive('getStatusCode')->twice()->andReturn(500);
        $r->shouldReceive('getReasonPhrase')->once()->andReturn('Big error!');
        $r->shouldReceive('getBody->getContents')->once()->andReturn('');
        $l->shouldReceive('error')->once()->with('Response error', ['code' => 500, 'msg' => 'Big error!', 'body' => '']);
        $this->expectException(ResponseException::class);
        $sut->process($r);
    }

    public function testResponseNotFoundIsException()
    {
        $r = m::mock(ResponseInterface::class);
        list($sut, $p, $l) = $this->getSut();
        $r->shouldReceive('getStatusCode')->twice()->andReturn(404);
        $r->shouldReceive('getReasonPhrase')->once()->andReturn('Not found');
        $r->shouldReceive('getBody->getContents')->once()->andReturn('');
        $this->expectException(NotFoundHttpException::class);
        $sut->process($r);
    }

    public function testAccessDeniedIsException()
    {
        $r = m::mock(ResponseInterface::class);
        list($sut, $p, $l) = $this->getSut();
        $r->shouldReceive('getStatusCode')->twice()->andReturn(401);
        $r->shouldReceive('getReasonPhrase')->once()->andReturn('Login');
        $r->shouldReceive('getBody->getContents')->once()->andReturn('');
        $this->expectException(AccessDeniedHttpException::class);
        $sut->process($r);
    }

    public function testNotAuthorizedIsException()
    {
        $r = m::mock(ResponseInterface::class);
        list($sut, $p, $l) = $this->getSut();
        $r->shouldReceive('getStatusCode')->twice()->andReturn(403);
        $r->shouldReceive('getReasonPhrase')->once()->andReturn('Not auth');
        $r->shouldReceive('getBody->getContents')->once()->andReturn('');
        $this->expectException(UnauthorizedHttpException::class);
        $sut->process($r);
    }

    public function testOkAndBodyProcessorExceptionIsException()
    {
        $r = m::mock(ResponseInterface::class);
        list($sut, $p, $l) = $this->getSut();
        $r->shouldReceive('getStatusCode')->once()->andReturn(200);
        $r->shouldReceive('getBody->getContents')->andReturn('foo');
        $e = new ResponseException('foo');
        $p->shouldReceive('process')->once()->with($r)->andThrow($e);
        $l->shouldReceive('error')->once()->with($e);
        $this->expectException(ResponseException::class);
        $sut->process($r);
    }

    public function testOkReturnsBodyProcessorResults()
    {
        $r = m::mock(ResponseInterface::class);
        list($sut, $p, $l) = $this->getSut();
        $r->shouldReceive('getStatusCode')->once()->andReturn(200);
        $r->shouldReceive('getBody->getContents');
        $p->shouldReceive('process')->once()->with($r)->andReturn('processed');
        $this->assertEquals('processed', $sut->process($r));
    }

    protected function getSut()
    {
        $p = m::mock(BodyProcessor::class);
        $l = m::mock(Log::class);
        $sut = new ResponseProcessor($p, $l);
        return [$sut, $p, $l];
    }
}
