<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 7:37 AM
 */

namespace Tests\Smorken\HttpModel\unit\HttpClient;

use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\HttpModel\Contracts\HttpClient\Connection;
use Smorken\HttpModel\Contracts\HttpClient\Model;
use Smorken\HttpModel\HttpClient\Request;
use Smorken\HttpModel\HttpClient\RequestException;
use Smorken\HttpModel\HttpClient\RequestMethodException;

class RequestTest extends \PHPUnit_Framework_TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testNoUriIsException()
    {
        list($sut, $conn) = $this->getSut();
        $this->expectException(RequestException::class);
        $sut->run();
    }

    public function testInvalidUriIsException()
    {
        list($sut, $conn) = $this->getSut();
        $this->expectException(RequestException::class);
        $sut->uri('Ö');
    }

    public function testBasicRequestIsGet()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['get', 'foo', []];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->toRequestOptions()
        );
        $this->assertEquals(Request::QUERYSTRING, $sut->getDefaultParamType());
    }

    public function testBasicPost()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['post', 'foo', []];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->post()
                ->toRequestOptions()
        );
        $this->assertEquals(Request::BODY, $sut->getDefaultParamType());
    }

    public function testBasicPut()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['put', 'foo', []];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->put()
                ->toRequestOptions()
        );
        $this->assertEquals(Request::BODY, $sut->getDefaultParamType());
    }

    public function testBasicPatch()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['patch', 'foo', []];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->patch()
                ->toRequestOptions()
        );
        $this->assertEquals(Request::BODY, $sut->getDefaultParamType());
    }

    public function testBasicDelete()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['delete', 'foo', []];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->delete()
                ->toRequestOptions()
        );
        $this->assertEquals(Request::QUERYSTRING, $sut->getDefaultParamType());
    }

    public function testBadMethodIsException()
    {
        list($sut, $conn) = $this->getSut();
        $this->expectException(RequestMethodException::class);
        $sut->uri('foo')
            ->dance()
            ->toRequestOptions();
    }

    public function testGetWithQueryStringParams()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['get', 'foo?fiz=buz', []];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->param('fiz', 'buz')
                ->toRequestOptions()
        );
    }

    public function testGetWithQueryStringParamsEncodes()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['get', 'foo?fiz=buz+baz', []];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->param('fiz', 'buz baz')
                ->toRequestOptions()
        );
    }

    public function testGetWithParamsInBodyPutsInQueryString()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['get', 'foo?fiz=buz&bar=baz', []];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->param('fiz', 'buz')
                ->param('bar', 'baz', Request::BODY)
                ->toRequestOptions()
        );
    }

    public function testPostWithDefaultAndSetParams()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['post', 'foo', ['form_params' => ['fiz' => 'buz', 'bar' => 'baz']]];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->post()
                ->param('fiz', 'buz')
                ->param('bar', 'baz', Request::BODY)
                ->toRequestOptions()
        );
    }

    public function testPostWithMixedParams()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['post', 'foo?fiz=buz', ['form_params' => ['bar' => 'baz']]];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->post()
                ->param('fiz', 'buz', Request::QUERYSTRING)
                ->param('bar', 'baz', Request::BODY)
                ->toRequestOptions()
        );
    }

    public function testPostWithParamsArray()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['post', 'foo?fiz=buz', ['form_params' => ['bar' => 'baz']]];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->post()
                ->params(['fiz' => 'buz'], Request::QUERYSTRING)
                ->params(['bar' => 'baz'], Request::BODY)
                ->toRequestOptions()
        );
    }

    public function testPostAsJsonBody()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['post', 'foo', ['json' => ['fiz' => 'buz', 'bar' => 'baz']]];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->post()
                ->json()
                ->param('fiz', 'buz')
                ->param('bar', 'baz', Request::BODY)
                ->toRequestOptions()
        );
    }

    public function testGetWithHeader()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['get', 'foo', ['headers' => ['Content-Type' => 'application/json']]];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->header('Content-Type', 'application/json')
                ->toRequestOptions()
        );
    }

    public function testGetWithHeaders()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['get', 'foo', ['headers' => ['Content-Type' => 'application/json']]];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->headers(['Content-Type' => 'application/json'])
                ->toRequestOptions()
        );
    }

    public function testGetWithOption()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['get', 'foo', ['timeout' => 2]];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->option('timeout', 2)
                ->toRequestOptions()
        );
    }

    public function testRunPassesOptionsToConnection()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['get', 'foo', ['timeout' => 2]];
        $conn->shouldReceive('send')->once()->with($expected)->andReturn('response');
        $this->assertEquals(
            'response',
            $sut->uri('foo')
                ->option('timeout', 2)
                ->run()
        );
    }

    public function testGetWithOptions()
    {
        list($sut, $conn) = $this->getSut();
        $expected = ['get', 'foo', ['timeout' => 2]];
        $this->assertEquals(
            $expected,
            $sut->uri('foo')
                ->options(['timeout' => 2])
                ->toRequestOptions()
        );
    }

    public function testFirstWithoutModelIsException()
    {
        list($sut, $conn) = $this->getSut();
        $conn->shouldReceive('send')->never();
        $this->expectExceptionMessage('Model must be set');
        $sut->uri('foo')
            ->option('timeout', 2)
            ->first();
    }

    public function testAllWithoutModelIsException()
    {
        list($sut, $conn) = $this->getSut();
        $conn->shouldReceive('send')->never();
        $this->expectExceptionMessage('Model must be set');
        $sut->uri('foo')
            ->option('timeout', 2)
            ->all();
    }

    public function testFirstWithoutResultIsNull()
    {
        $m = m::mock(Model::class);
        list($sut, $conn) = $this->getSut($m);
        $expected = ['get', 'foo', ['timeout' => 2]];
        $conn->shouldReceive('send')->once()->with($expected)->andReturn(null);
        $m->shouldReceive('getParser')->andReturn(null);
        $this->assertNull(
            $sut->uri('foo')
                ->option('timeout', 2)
                ->first()
        );
    }

    public function testFirstWithResultIsFirstModel()
    {
        $m = m::mock(Model::class);
        list($sut, $conn) = $this->getSut($m);
        $expected = ['get', 'foo', ['timeout' => 2]];
        $conn->shouldReceive('send')->once()->with($expected)->andReturn([['bar' => 'foo']]);
        $m->shouldReceive('newInstance')->with(['bar'=> 'foo'])->andReturn('model');
        $m->shouldReceive('getParser')->andReturn(null);
        $this->assertEquals(
            'model',
            $sut->uri('foo')
                ->option('timeout', 2)
                ->first()
        );
    }

    public function testAllWithoutResultIsEmptyCollection()
    {
        $m = m::mock(Model::class);
        list($sut, $conn) = $this->getSut($m);
        $expected = ['get', 'foo', ['timeout' => 2]];
        $conn->shouldReceive('send')->once()->with($expected)->andReturn(null);
        $m->shouldReceive('getParser')->andReturn(null);
        $this->assertCount(
            0,
            $sut->uri('foo')
                ->option('timeout', 2)
                ->all()
        );
    }

    public function testAllWithoutResultIsCollection()
    {
        $m = m::mock(Model::class);
        list($sut, $conn) = $this->getSut($m);
        $expected = ['get', 'foo', ['timeout' => 2]];
        $conn->shouldReceive('send')->once()->with($expected)->andReturn([['bar' => 'foo']]);
        $m->shouldReceive('newInstance')->with(['bar' => 'foo'])->andReturn('model1');
        $m->shouldReceive('getParser')->andReturn(null);
        $coll = $sut->uri('foo')
            ->option('timeout', 2)
            ->all();
        $this->assertInstanceOf(Collection::class, $coll);
        $this->assertCount(1, $coll);
        $this->assertEquals('model1', $coll->first());
    }

    protected function getSut($model = null)
    {
        $conn = m::mock(Connection::class);
        $sut = new Request($conn);
        if ($model) {
            $sut->setModel($model);
        }
        return [$sut, $conn];
    }
}
