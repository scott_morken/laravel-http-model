<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 3:34 PM
 */

namespace Tests\Smorken\HttpModel\unit\Soap\Parsers\Phpro;

use Smorken\HttpModel\Soap\Parsers\Phpro\VoToArray;

class VoToArrayTest extends \PHPUnit_Framework_TestCase
{

    public function testWithArray()
    {
        $data = [
            'foo' => 'bar',
            'fiz' => [
                'baz' => 'buz',
            ],
        ];
        $r = $this->getSut()->parse($data);
        $this->assertEquals($data, $r);
    }

    public function testWithOneModel()
    {
        $m = new Model1;
        $m->id = 1;
        $m->foo = 'foobar';
        $this->assertEquals(['Id' => 1, 'Foo' => 'foobar', 'Model2' => null], $this->getSut()->parse($m));
    }

    public function testWithArrayOfModels()
    {
        $m = new Model1;
        $m->id = 1;
        $m->foo = 'foobar';
        $m2 = new Model1;
        $m2->id = 2;
        $m2->foo = 'foobar2';
        $this->assertEquals(
            [['Id' => 1, 'Foo' => 'foobar', 'Model2' => null], ['Id' => 2, 'Foo' => 'foobar2', 'Model2' => null]],
            $this->getSut()->parse([$m, $m2])
        );
    }

    public function testWithNestedModel()
    {
        $m = new Model1;
        $m->id = 1;
        $m->foo = 'foobar';
        $m2 = new Model2;
        $m2->id = 1;
        $m2->bar = 'barfoo';
        $m->model2 = $m2;
        $this->assertEquals(
            ['Id' => 1, 'Foo' => 'foobar', 'Model2' => ['Id' => 1, 'Bar' => 'barfoo', 'Model3s' => null]],
            $this->getSut()->parse($m)
        );
    }

    public function testWithNestedModelAndModels()
    {
        $m = new Model1;
        $m->id = 1;
        $m->foo = 'foobar';
        $m2 = new Model2;
        $m2->id = 1;
        $m2->bar = 'barfoo';
        $m->model2 = $m2;
        $m3 = new Model3;
        $m3->id = 1;
        $m3->fiz = 'fiz1';
        $m31 = new Model3;
        $m31->id = 2;
        $m31->fiz = 'fiz2';
        $m2->model3s = [$m3, $m31];
        $this->assertEquals(
            [
                'Id'     => 1,
                'Foo'    => 'foobar',
                'Model2' => [
                    'Id'      => 1,
                    'Bar'     => 'barfoo',
                    'Model3s' => [
                        ['Id' => 1, 'Fiz' => 'fiz1'],
                        ['Id' => 2, 'Fiz' => 'fiz2'],
                    ],
                ],
            ],
            $this->getSut()->parse($m)
        );
    }

    protected function getSut()
    {
        return new VoToArray();
    }
}

class Model1
{

    protected $id;
    protected $foo;
    protected $model2;

    public function getId()
    {
        return $this->id;
    }

    public function getFoo()
    {
        return $this->foo;
    }

    public function getModel2()
    {
        return $this->model2;
    }

    public function __set($k, $v)
    {
        $this->$k = $v;
    }
}

class Model2
{

    protected $id;
    protected $bar;
    protected $model3s = [];

    public function getId()
    {
        return $this->id;
    }

    public function getBar()
    {
        return $this->bar;
    }

    public function getModel3s()
    {
        return $this->model3s;
    }

    public function __set($k, $v)
    {
        $this->$k = $v;
    }
}

class Model3
{

    protected $id;
    protected $fiz;

    public function getId()
    {
        return $this->id;
    }

    public function getFiz()
    {
        return $this->fiz;
    }

    public function __set($k, $v)
    {
        $this->$k = $v;
    }
}
